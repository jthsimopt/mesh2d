classdef Mesh2D < matlab.mixin.Copyable
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        P
        T
        nele
        nnod
        hSize
        neighs
        Element
    end
    
    properties (Access = private)
        nxe
        nye
        eleType
        x0
        x1
        y0
        y1
        
    end
    
    methods
        % constructor
        function O = Mesh2D(x0,x1,y0,y1,nxe,nye,eleType,eleOrder)
            [O.T,O.P,O.neighs,O.Element] = CreateMesh(x0,x1,y0,y1,nxe,nye,eleType,eleOrder);
            O.nele = size(O.T,1);
            O.nnod = size(O.P,1);
            O.nxe = nxe;
            O.nye = nye;
            O.eleType = eleType;
            O.x0 = x0; O.x1 = x1;
            O.y0 = y0; O.y1 = y1;
            O.hSize = ((x1-x0)/nxe * (y1-y0)/nye)^.5;
        end
        
        function h = Visualize(O,varargin)
            % h = Visualize()
            
            %% Check for xfigure
            if ~isempty(findall(0,'type','figure')) && ishold
                %g�r inget
            elseif exist('xfigure','file') == 2
                h.fig = xfigure;
            else
                RequiredFileMissing('xfigure', 'https://raw.githubusercontent.com/cenmir/xfigure/master/xfigure.m')
                RequiredFileMissing('xfigure_KPF', 'https://raw.githubusercontent.com/cenmir/xfigure/master/xfigure_KPF.m')
                h.fig = xfigure;
            end
            
            %% Get eleType 
            iEleType = find(strcmpi(ValidEleTypes,O.eleType), 1);
            switch iEleType
                case 1 %quad
                    nodes = O.T(:,1:4);
                case 2 %tri
                    nodes = O.T(:,1:3);
                case 3 %fishTri
                    nodes = O.T(:,1:3);
            end
            
            %% Draw the patch
            h.patch = patch('Faces',nodes,'Vertices',O.P, 'FaceColor','w');
            axis equal; axis(axispad([min(O.P(:,1)),max(O.P(:,1)),min(O.P(:,2)),max(O.P(:,2))],0.1))
            xlabel('X'); ylabel('Y');
            h.fig.Color = 'w';
            
            
            if isenabled('NodeNumbers',varargin)
                if O.nele > 100
                    warning('Cannot draw NodeNumbers, too many elements')
                    return
                end
                h.NodeText = [];
                %                 unique(T.Connectivity(:),'stable')'
                %                 1:T.nnod
                for i = 1:O.nnod
                    h.NodeText = [h.NodeText; text(O.P(i,1),O.P(i,2),num2str(i),'BackgroundColor','w') ];
                end
            end
            
            if isenabled('ElementNumbers',varargin)
                if O.nele > 100
                    warning('Cannot draw ElementNumbers, too many elements')
                    return
                end
                
                xnod = O.P(:,1); ynod = O.P(:,2);
                h.EleText = [];
                if size(O.T,2) == 9 %quadP2
                    dx = (O.x1-O.x0)/O.nxe;
                    dy = (O.y1-O.y0)/O.nye;
                    for i = 1:O.nele
                        xm = mean(xnod(nodes(i,:))) - dx/4;
                        ym = mean(ynod(nodes(i,:))) + dy/4;
                        h.EleText = [h.EleText; text(xm,ym,num2str(i),'BackgroundColor','y')];
                    end
                else
                    for i = 1:O.nele
                        xm = mean(xnod(nodes(i,:)));
                        ym = mean(ynod(nodes(i,:)));
                        h.EleText = [h.EleText; text(xm,ym,num2str(i),'BackgroundColor','y')];
                    end
                end
                
            end
            
           
        end
        
        function PeriodicBoundary(O,dim)
            % O.PeriodicBoundary(dim)
            % Converts the mesh to a periodic mesh by merging the boundary
            % nodes. 
            % The boundary to be merged are specified by the string dim
            % Valid values are 'X', 'Y', 'XY' or 'YX'
            
            %% Input handling
            validTypes = {'X','Y','XY','YX'};
            iDimType = find(strcmpi(validTypes,dim));
            if isempty(iDimType)
                sValidTypes = '';
                for i = 1:length(validTypes)
                    sValidTypes = [sValidTypes,'"',validTypes{i},'" ,'];
                end
                error(['Wrong element type. Valid element types are: ', sValidTypes])
            end
            
            %%
            xnod = O.P(:,1);
            ynod = O.P(:,2);
            nodes = O.T;

            %%
            switch iDimType
                case 1
                    indx1=find(xnod == O.x0);
                    indx2=find(xnod == O.x1);
                    for c=[indx1';indx2']
                        nodes(nodes==c(2)) = c(1);
                    end
                case 2
                    indy1=find(ynod == O.y0);
                    indy2=find(ynod == O.y1);
                    for c=[indy1';indy2']
                        nodes(nodes==c(2)) = c(1);
                    end
                case 3
                    indx1=find(xnod == O.x0);
                    indx2=find(xnod == O.x1);
                    indy1=find(ynod == O.y0);
                    indy2=find(ynod == O.y1);
                    for c=[indx1';indx2']
                        nodes(nodes==c(2)) = c(1);
                    end
                    for c=[indy1';indy2']
                        nodes(nodes==c(2)) = c(1);
                    end
                case 4
                    indx1=find(xnod == O.x0);
                    indx2=find(xnod == O.x1);
                    indy1=find(ynod == O.y0);
                    indy2=find(ynod == O.y1);
                    for c=[indx1';indx2']
                        nodes(nodes==c(2)) = c(1);
                    end
                    for c=[indy1';indy2']
                        nodes(nodes==c(2)) = c(1);
                    end
            end
            
            %% Fix missing nodes
            tt = nodes(:);
            [nele,s]=size(nodes);
            ii = 1:max(tt);
            miss = find(~ismember(ii,nodes))';
            xnod(miss)=[];ynod(miss)=[];
            for i=1:length(miss)
                ind=find(tt > miss(i));
                tt(ind)=tt(ind)-1;
                miss=miss-1;
            end
            ending=max(tt);
            xnod(ending+1:end)=[];ynod(ending+1:end)=[];
            
            O.T = reshape(tt,nele,s);
            O.P = [xnod,ynod];
            O.nnod = size(O.P,1);
            O.nele = size(O.T,1);
        end
        
    end
    
    %Hide some of the inherited methods from handle
    methods(Hidden)
      function lh = addlistener(varargin)
         lh = addlistener@handle(varargin{:});
      end
      function notify(varargin)
         notify@handle(varargin{:});
      end
      function delete(varargin)
         delete@handle(varargin{:});
      end
      function Hmatch = findobj(varargin)
         Hmatch = findobj@handle(varargin{:});
      end
      function p = findprop(varargin)
         p = findprop@handle(varargin{:});
      end
      function TF = eq(varargin)
         TF = eq@handle(varargin{:});
      end
      function TF = ne(varargin)
         TF = ne@handle(varargin{:});
      end
      function TF = lt(varargin)
         TF = lt@handle(varargin{:});
      end
      function TF = le(varargin)
         TF = le@handle(varargin{:});
      end
      function TF = gt(varargin)
         TF = gt@handle(varargin{:});
      end
      function TF = ge(varargin)
         TF = ge@handle(varargin{:});
      end
   end
    
end
 
function validEleTypes = ValidEleTypes()
    validEleTypes = {'quad','tri', 'fishTri'};
end

function [T,P,neighs,Element] = CreateMesh(x0,x1,y0,y1,nxe,nye,eleType,eleOrder)

validEleTypes = ValidEleTypes();

%% Choose what to do based on element type
iEleType = find(strcmpi(validEleTypes,eleType), 1);
if isempty(iEleType)
    sValidEleTypes = '';
    for i = 1:length(validEleTypes)
        sValidEleTypes = [sValidEleTypes,'"',validEleTypes{i},'" ,'];
    end
    sValidEleTypes = sValidEleTypes(1:end-2);
    error(['Wrong element type. Valid element types are: ', sValidEleTypes])
end

switch eleOrder
    case 1
        [P,nodes] = CreatePoints(x0,x1,y0,y1,nxe,nye);
        switch iEleType
            case 1 %quad
                T = nodes;
                neighs = [];
                Element = [];
            case 2 %tri
                T = Quads2Tri(nodes);
                neighs = [];
                Element = [];
            case 3 %fishTri
                T = Quads2Fish(nodes,nxe,nye);
                neighs = [];
                Element = [];
        end
    case 2
        [P,nodes] = CreatePoints(x0,x1,y0,y1,nxe,nye);
        nele = size(nodes,1);
        

        %% Preallocate
        element.edges = zeros(4,2);
        element.nodes = [0,0,0,0];
        element.neighs = [0,0,0,];
        Element = repmat(element,nele,1); %Fills all triangle elements.
        %% Neighbors
        [neighs, Element] = Neighbors(Element,nele,nxe,nye);
        
        
        %% Refine
        [nodes,P,Element] = RefineMesh(nodes,P,neighs,Element);
        nele = size(nodes,1);
        
        switch iEleType
            case 1 %quad
                T = nodes;
            case 2 %tri
                T = zeros(2*nele,6);
                for iel = 1:nele
                    T(2*iel-1,:) = nodes(iel,[1,2,3,5,6,9]);
                    T(2*iel-0,:) = nodes(iel,[1,3,4,9,7,8]);
                end
                Element = [];
                neighs = [];
            case 3 %fishTri
                T = zeros(2*nele,6);
                iel = 1;
                for j = 1:nxe
                    for i = 1:nye
                        forwardx = logical(mod(j,2));
                        forwardy = logical(mod(i,2));
                        forward = forwardx && forwardy;
                        if (~forwardx) && (~forwardy)
                            forward = true;
                        end
                        if forward
                            T(2*iel-1,:) = nodes(iel,[1,2,3,5,6,9]);
                            T(2*iel-0,:) = nodes(iel,[1,3,4,9,7,8]);
                        else
                            T(2*iel-1,:) = nodes(iel,[1,2,4,5,9,8]);
                            T(2*iel-0,:) = nodes(iel,[2,3,4,6,7,9]);
                        end
                        iel = iel+1;
                    end
                end
                Element = [];
                neighs = [];
                
        end
    otherwise
        error(['Polynomial order ',num2str(eleOrder),' not yet implemented! Max order is 2.'])
end



end

function [nodes2,P,Element] = RefineMesh(nodes,P,neighs,Element)
    nele = size(nodes,1);
    nnods = size(P,1);
    ele = 1:nele;
    xnod = P(:,1); ynod = P(:,2);
    nmax = nnods;
        
    P2 = NaN(5*nnods,2);
    P2(1:nmax,:) = P;
    nodes2 = zeros(nele,9);
    for iel = ele
        iv = nodes(iel,:);
        edges = iv([1,2;2,3;3,4;4,1]);
        Element(iel).edges = edges;
        Element(iel).nodes = iv;

        XN = zeros(5,2);   % 5 New points
        XN(1:4,:) = (P2(edges(:,1),:)+P2(edges(:,2),:))/2;
        XN(5,:) = [mean(xnod(iv)),mean(ynod(iv))];
        newNods = (nmax+1:nmax+5);
        
        newNods2 = newNods;
        N = neighs(iel,:);  N(N==0)=[];
        prevNeigh = N(iel > N);
        if ~isempty(prevNeigh)
            nods = nodes2(prevNeigh,5:end-1);
            searchInds = nods(:);
            searchSpace = P2(searchInds,:);
            testNods = newNods(1:end-1);
            testSpace = XN(1:4,:);

            nSearch = size(searchSpace,1);
            Inds2Remove = [0;0;0;0];
            for itest = 1:4
                ip = testSpace(itest,:);
                sres = searchInds(all(ip(ones(nSearch,1),:)==searchSpace,2));
                if ~isempty(sres)
                    testNod = testNods(itest);
                    Inds2Remove(itest) = 1;
                    nI = find(newNods2==testNod);
%                     newNods2 = newNods;
                    newNods2(nI) = sres;
                    newNods2(nI+1:5) = newNods2(nI+1:5)-1;
                    testNods = newNods2(1:4);
                end
            end
            XN(logical(Inds2Remove),:) = [];
        end

        locnod = [iv,newNods2];
        Element(iel).nodes = locnod;
        nodes2(iel,:) = locnod;
        lo = nmax+1;
        up = nmax+size(XN,1);
        P2(lo:up,:) = XN;
        nmax = nmax+size(XN,1);

%         xfigure(23); hold on;
%         patch('Faces',nodes2(1:iel,1:4),'Vertices',P,'FaceColor','w')
%         for i=1:size(P2,1)
%             text(P2(i,1),P2(i,2),num2str(i), 'BackgroundColor','w');
%         end
%         for ii=1:size(nodes2(all(nodes2>0,2),1:4),1)
%             ivv = nodes2(ii,1:4);
%             xm = mean(P2(ivv,1))-0.1; ym = mean(P2(ivv,2))+0.1;
%             text(xm,ym,num2str(ii), 'BackgroundColor','y');
%         end
%         nodes2(iel,:)
%         1;
    end
    P2(all(isnan(P2),2),:)=[];
    P = P2;
    
%     xfigure(23); hold on;
%     patch('Faces',nodes2(:,1:4),'Vertices',P,'FaceColor','w')
%     for i=1:size(P,1)
%         text(P(i,1),P(i,2),num2str(i), 'BackgroundColor','w');
%     end
%     for ii=1:size(nodes2(all(nodes2>0,2),1:4),1)
%         ivv = nodes2(ii,1:4);
%         xm = mean(P(ivv,1)); ym = mean(P(ivv,2));
%         text(xm,ym,num2str(ii), 'BackgroundColor','y');
%     end
%     1;
    
end

function [neighs, Element] = Neighbors(Element,nele,nxe,nye)
    neighs = zeros(nele,4);
    iel = 1;
    for j = 1:nxe
        for i = 1:nye
            % y-dir
            if i==nye && i == 1
                n1 = 0;
                n2 = 0;
            elseif i==nye
                n1 = 0;
                n2 = iel-1;
            elseif i==1
                n1 = iel+1;
                n2 = 0;
            else
                n1 = iel+1;
                n2 = iel-1;
            end

            %x-dir
            if j==nxe && j==1
                n3 = 0;
                n4 = 0;
            elseif j==nxe
                n3 = 0;
                n4 = iel-nye;
            elseif j==1
                n3 = iel+nye;
                n4 = 0;
            else
                n3 = iel+nye;
                n4 = iel-nye;
            end

            neighs(iel,:) = [n1,n2,n3,n4];
            Element(iel).neighs = [n1,n2,n3,n4];
            iel = iel+1;
        end
    end
end

function [P,nodes] = CreatePoints(x0,x1,y0,y1,nxe,nye)
    %% Creating grid points
    % Using meshgrid to create P matrix nPoints-by-2
    [X,Y] = meshgrid(linspace(x0,x1,nxe+1),linspace(y0,y1,nye+1));
    P = [X(:),Y(:)];
    nnods = size(P,1);

    %% Creating connectivity matrix
    % nodes matrix, nele-by-4
    nodes = zeros(nye*nxe,4);
    M = reshape(1:nnods,nye+1,nxe+1);
    c = 1;
    for j = 1:nxe
        for i = 1:nye
            ii = i:i+1;
            jj = j:j+1;
            ee = M(ii,jj);
            nodes(c,:) = ee([1,3,4,2])';
            c = c+1;
        end
    end
end

function tri = Quads2Tri(nodes) 
    
    nele = size(nodes,1);
    tri = zeros(2*nele,3);
    for iel = 1:nele
        tri(2*iel-1,:) = nodes(iel,[1,2,3]);
        tri(2*iel-0,:) = nodes(iel,[1,3,4]);
    end
end

function tri = Quads2Fish(nodes,nxe,nye)
    nele = size(nodes,2);
    tri = zeros(2*nele,3);
    iel = 1;
    for j = 1:nxe
        for i = 1:nye
%             iel
            forwardx = logical(mod(j,2));
            forwardy = logical(mod(i,2));
            forward = forwardx && forwardy;
            if (~forwardx) && (~forwardy)
                forward = true;
            end
%             forward
%             1;
            if forward
                tri(2*iel-1,:) = nodes(iel,[1,2,3]);
                tri(2*iel-0,:) = nodes(iel,[1,3,4]);
            else
                tri(2*iel-1,:) = nodes(iel,[1,2,4]);
                tri(2*iel-0,:) = nodes(iel,[2,3,4]);
            end

            iel = iel+1;
        end
    end

end

function X = axispad(X,p)
    X(1:2:end) = X(1:2:end)-p;
    X(2:2:end) = X(2:2:end)+p;
end

function RequiredFileMissing(filename, RemoteDestination)
    %If we're going to download a whole bunch of files, it is better to set
    % RequiredFilesDir to be a global and not have to ask the user to
    % specify a destination folder for every file...
    global RequiredFilesDir
    
    
    disp([filename,' is missing!'])
    disp(['Trying to download ',filename,' from ',RemoteDestination])
    
    
    if isempty(RequiredFilesDir)
        scriptPath = mfilename('class');
        [ScriptDir,~,~] = fileparts(scriptPath);
        DestDir = uigetdir(ScriptDir,['Select where to save ',filename,'. Make sure its either the script directory or a directory on the Path.']);
        if DestDir == 0
            error(['Failed to select folder, failed to install ',filename])
        end
        
        RequiredFilesDir = DestDir;
    end
    DestFile= [RequiredFilesDir,'/',filename];
    
    % Download the RemoteFile and save it to DestFile
    websave(DestFile,RemoteDestination);
    
    % Give up to 10 seconds for the file to show up, otherwise send error
    % message.
    tic
    while 1
        if exist('xfigure','file') == 2
            break
        end
        pause(0.1)
        t1 = toc;
        if t1 > 10
            error(['Failed to download ',filename,'! Timeout.'])
        end
    end

    
end

function rv = isenabled(mode, varargin)
    %   ISENABLED  Checks if mode exists in the cell-array varargin.
    %
    %   isenabled(mode,varargin{:}) return true or false.
    %   example:
    %
    %          varargin = {'Viz', 'ElementNumber', 'debug', [20,20]};
    %          isenabled('debug',varargin)
    %          ans =
    %               1
    %
    %   Author: Mirza Cenanovic (mirza.cenanovic@jth.hj.se)
    %   Date: 2013-05-02
    if nargin < 1
        error('No arguments')
    end
    varargin = varargin{:};

    ind = find(strcmpi(varargin,mode), 1);
    if ~isempty(ind)
        rv = 1;
    else
        rv = 0;
    end
end






