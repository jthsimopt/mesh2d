# Mesh2D
*Mesh2D* class. This is fast.

## Updates
| Date| Change |
| :---| ------ |
|2016-05-25| Can generate both P1 and P2 elements. An additional argument is added to the constructor.|

## Demo

	x0 = 0; x1 = 4; y0 = 0; y1 = 2;
	nxe = 4; nye = 3; order = 2;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'fishtri', order)
	M.Visualize('ElementNumbers', 'NodeNumbers')

## Todo:
- Neighbors for triangles
- Local refinement
- Hanging nodes
- Element structure for triangles
- Basefunction
- Quadrature rules


## Properties
| Function Name   | Description |
|-----------------|-------------|
| `T`  | Element connectivity list, a.k.a. the topology, is an m-by-n matrix where *m* is the number of elements and *n* is the number of nodes for the perticular element type. Each element in *T* is a vertex ID. Each row of *T* contains the vertex IDs that define an element.  |
| `P`        | Points or Vertices, specified as a matrix whose columns are the x and y coordinates of the mesh points. The row numbers of *P* are the vertex IDs in the connectivity matrix *T*.|
| `nele`      | Numner of elements.
| `nnod`    | Number of nodes|
| `hSize`   | Element size defined as the diagonal length of a quad element |
| `Element` | Element struct containing per element information. Contains the fiels **`edges`**, **`nodes`** and **`neighs`**.|
| `neighs`  | Element neighbor matrix. It's a *m*-by-*n* matrix where *m* is the number of elements and *n* the number of nodes per element. Each row of *neighs* contains element IDs of the neighboring elements. |


## Functions

| Function Name   | Description |
|-----------------|-------------|
| `Mesh2D()`      | The constructor, creates the *Mesh2D* object |
| `Visualize()`   | Creates a visualization in a figure  |
| `PeriodicBoundary(dim)` | Converts the mesh to a periodic mesh by merging the boundary nodes. `dim` can be set to `'X'`, `'Y'`, `'XY'` or `'YX'`|

### Mesh2D() - Description

**`MO = Mesh2D(x0,x1,y0,y1,nxe,nye,ElementType)`**
Creates the *Mesh2D* object `MO` using the bounding coordinates `x0, x1, y0, y1`, number of elements along the x-, and y-direction `nxe` and `nye` and the specified `ElementType`. 

**`x0, x1, y0, y1`**
Scalar values defining the lower and upper bounds of the mesh.

**`nxe, nye`**
Scalar values defining the number of elements in each direction.

**`ElementType`**
Defines the element type.

- `'quad'` - Bi-linear quadliteral element.
- `'tri'`  - Structured linear triangle element.
- `'fishTri'` - Semi-structured linear triangle element "fishbone". 

### Visualize() - Description

**`h = Visualize()`**
Creates a visualization in a figure and outputs all graphical handles into the struct `h`.

`h = Visualize(properties)`

*properties*:

- *'NodeNumbers'* - Draws node numbers.
- *'ElementNumbers'* - Draws element numbers.

## Mesh types
### Bi-linear quad
	x0 = 0; x1 = 1; y0 = 0; y1 = 1;
	nxe = 4; nye = 4; order = 1;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'quad',order)
	M.Visualize('ElementNumbers', 'NodeNumbers')
![](https://bytebucket.org/jthsimopt/mesh2d/raw/b9aff2f3bab35c9f68abe805c851d328c89878f9/Graphics/quad.png)

### Structured triangle 
	x0 = 0; x1 = 1; y0 = 0; y1 = 1;
	nxe = 4; nye = 4; order = 1;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'tri',order)
	M.Visualize('ElementNumbers', 'NodeNumbers')
![](https://bytebucket.org/jthsimopt/mesh2d/raw/b9aff2f3bab35c9f68abe805c851d328c89878f9/Graphics/tri.png)

### Fishbone triangle
	x0 = 0; x1 = 1; y0 = 0; y1 = 1;
	nxe = 4; nye = 4; order = 1;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'fishTri',order)
	M.Visualize('ElementNumbers', 'NodeNumbers')
![](https://bytebucket.org/jthsimopt/mesh2d/raw/b9aff2f3bab35c9f68abe805c851d328c89878f9/Graphics/fishbone.png)

### Second order Quad
	x0 = 0; x1 = 1; y0 = 0; y1 = 1;
	nxe = 4; nye = 4; order = 2;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'quad',order)
	M.Visualize('ElementNumbers', 'NodeNumbers')
![](https://bytebucket.org/jthsimopt/mesh2d/raw/2b95f6983afe394eb773bdd6fa19491d7b3b549f/Graphics/quad2.png)

### Second order structured triangle
	x0 = 0; x1 = 1; y0 = 0; y1 = 1;
	nxe = 4; nye = 4; order = 2;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'tri',order)
	M.Visualize('ElementNumbers', 'NodeNumbers')
![](https://bytebucket.org/jthsimopt/mesh2d/raw/2b95f6983afe394eb773bdd6fa19491d7b3b549f/Graphics/tri2.png)

### Second order fishbone triangle
	x0 = 0; x1 = 1; y0 = 0; y1 = 1;
	nxe = 4; nye = 4; order = 2;
	M = Mesh2D(x0,x1,y0,y1,nxe,nye,'fishTri',order)
	M.Visualize('ElementNumbers', 'NodeNumbers')
![](https://bytebucket.org/jthsimopt/mesh2d/raw/2b95f6983afe394eb773bdd6fa19491d7b3b549f/Graphics/fishbone2.png)